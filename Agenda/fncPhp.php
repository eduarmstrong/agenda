<?php
  class fncPhp {

    function fncPhp() {
      ini_set("memory_limit","1024");      
      $this->conn = mysql_pconnect('eduarmstrong:3306','edu','qa.4736');
      mysql_select_db('BR');
    }

    function fncLogin($ENT_NM_LOGIN) {
    
      return mysql_query
        ("
        SELECT
          BR100_NR_USR    AS NR_USR   ,
          BR100_NM_USR    AS NM_USR   ,
          BR100_NM_LOGIN  AS NM_LOGIN ,
          BR100_CD_SITUAC AS CD_SITUAC,
          BR100_CD_SENHA  AS CD_SENHA
        FROM
          BR100
        WHERE
          BR100_NM_LOGIN = '$ENT_NM_LOGIN'        
      ");
    }
    
    public function fncExecSQL($SQL) {
      
      // Executa o comando SQL
      $result = mysql_query(utf8_decode($SQL));
      
	  // Se ocorreu tudo certo devolve o resultSet do comando
	  if ($result)
	    return  $$arraY = array(0, 'Opera��o realizada com sucesso', $result);
	  // Se n�o volta o c�digo de erro
	  else 
	    return  $$arraY = array(- 1, 'Erro ao executar fncExecSQL', mysql_error());     
    }
    
    public function fncExecSP($ENT_NM_SP, $ENT_DS_PARAM) {      
      // Conex�o com banco de dados
      $conexao = mysqli_connect('eduarmstrong:3306', 'edu', 'qa.4736', 'BR');
      
      // Concatena a procedure com os par�metros
      $call = "CALL $ENT_NM_SP($ENT_DS_PARAM)";

      // Executa a procedure
      $resulSet = mysqli_query($conexao, utf8_decode($call));

      // Se ocorreu tudo certo devolve o resultSet da procedure
      if ($resulSet)
        return $array = array(0, 'Opera��o realizada com sucesso', $resulSet);
      // Se n�o volta o c�digo de erro 
      else
        return $array = array(-1, 'Erro ao excutar fncExecSP', mysqli_error($conexao));

    }
	  
    private function fncArrayToString($DADO) {
      return implode(',', $DADO);
    }
    /**
     * fncGravaArqConf - Grava arquivo.ini na pasta _conf
     * @param Conteudo $String
     * @param NomeArquivo $String
     * @return mensagem OK
    */
    public function fncGravaArqConf($Conteudo, $NomeArquivo, $LogErro) {
      try {	
        // Configura pasta + o nome do arquivo
        $nomeFile = "../../_conf/{$NomeArquivo}.ini";

        // Abre o arquivo. A op��o w vai cri�-lo, caso n�o exista 
        $arquivo = fopen($nomeFile, 'w');
      
        // Escreve no arquivo
        fwrite($arquivo, $Conteudo);
      
        // fecha o arquivo
        fclose($arquivo);
        // Array de retorno que ser� tratado pela aplica��o		
        $array = array(1, 'Ok', "{$NomeArquivo}.ini");      
      } 
      catch (Exception $e) {    
        if ($LogErro) {
          // Grava arquivo com mensagem de erro
          $trace = fopen("../../_trace/_{$NomeArquivo}.log", 'w');    
          // Escreve no arquivo
          fwrite($trace,  "C�digo: {$e->getCode()}");
          fwrite($trace,  "\n");
          fwrite($trace,  "Mensagem: {$e->getMessage()}");
          // fecha o arquivo
          fclose($trace);
        
          // Array de retorno com mensagem de erro
          $array = array(- 10, $e->getCode(), $e->getMessage());
        }
      }

      return $array;	  
    }
	
    /**
     * fncGravaJPG - Grava arquivo.jpg na pasta _download
     * @param Imagem $ByteArray
     * @param NomeArquivo $String
     * @return mensagem OK
    */
    public function fncGravaJPG($Imagem, $NomeArquivo, $LogErro) {
    
      //obt�m um id unico para o arquivo
      // $nomeArquivo = uniqid();
        try {
        //configura o nome do arquivo
      $nomeImagem = "../../_download/{$NomeArquivo}.jpg";

      //abre o arquivo. A op��o w vai cri�-lo, caso n�o exista
      $arquivo = fopen($nomeImagem, 'w');

      //escreve no arquivo
      fwrite($arquivo, base64_decode($Imagem));
        
      // fecha o arquivo
      fclose($arquivo);
      
          // Array de retorno que ser� tratado pela aplica��o		
      $array = array(1, 'Ok', "{$NomeArquivo}.jpg");
      
      } 
      catch (Exception $e) {
    
        if ($LogErro) {
        // Grava arquivo com mensagem de erro
        $trace = fopen("../../_trace/fncGravaJPG_{$NomeImg}.log", 'w');    
        // Escreve no arquivo
        fwrite($trace,  "C�digo: {$e->getCode()}");
        fwrite($trace,  "\n");
        fwrite($trace,  "Mensagem: {$e->getMessage()}");
        // fecha o arquivo
        fclose($trace);
      }
        // Array de retorno com mensagem de erro
        $array = array(- 10, $e->getCode(), $e->getMessage());
        }

      return $array;
    }
	
    public function fncDeleteFile($NomeArquivo, $Pasta, $LogErro) {

      try {
        //configura o nome do arquivo
        $arquivo = "../../{$Pasta}/{$NomeArquivo}";
        // Apaga o arquivo da pasta
        unlink($arquivo);
      
        // Array de retorno que ser� tratado pela aplica��o		
        $array = array(1, 'Ok', $NomeArquivo);
      }
      catch (Exception $e) {
      
        if ($LogErro) {
        // Grava arquivo com mensagem de erro
        $trace = fopen("../../_trace/fncDeleteFile_{$NomeArquivo}.log", 'w');    
        // Escreve no arquivo
        fwrite($trace,  "C�digo: {$e->getCode()}");
        fwrite($trace,  "\n");
        fwrite($trace,  "Mensagem: {$e->getMessage()}");
        fwrite($trace,  "\n");
        fwrite($trace,  "Pasta: {$Pasta}");
        // fecha o arquivo
        fclose($trace);
      }
        // Array de retorno com mensagem de erro
        $array = array(- 10, $e->getCode(), $e->getMessage());	  
      }

      return $array;	  
    }
	
    public function fncListarArquivo($Pasta, $LogErro) {		
      try {	
        // Abre o diret�rio passado por par�metro
        $diretorio = dir("../../{$Pasta}");
      
        // Enquanto houver arquivo na pasta ir� adicionaro no array
        while ($arquivo = $diretorio->read())
          // if para n�o adicionar . e ..	
          if ($arquivo != "." && $arquivo != "..")
            $array[] = $arquivo;
        
        if ($array == null)
          $array = array(0, "Ok", "Pasta vazia");
        
        $diretorio->close(); 
      }
      catch (Exception $e) {
      
        if ($LogErro) {
        // Grava arquivo com mensagem de erro
        $trace = fopen("../../_trace/fncListarArquivo_{$Pasta}.log", 'w');    
        // Escreve no arquivo
        fwrite($trace,  "C�digo: {$e->getCode()}");
        fwrite($trace,  "\n");
        fwrite($trace,  "Mensagem: {$e->getMessage()}");
        fwrite($trace,  "\n");
        fwrite($trace,  "Pasta: {$Pasta}");
        // fecha o arquivo
        fclose($trace);
      }
        // Array de retorno com mensagem de erro
        $array = array(- 10, $e->getCode(), $e->getMessage());	  
      }	  
      return $array;
    }
  }
?>